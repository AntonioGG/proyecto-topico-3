const btnDelete= document.querySelectorAll('.btn-delete');
if(btnDelete) {
  const btnArray = Array.from(btnDelete);
  btnArray.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      if(!confirm('¿Estas seguro de borrar el registro?')){
        e.preventDefault();
      }
    });
  })
}
window.addEventListener('mousewheel', function(e){
    e.preventDefault();
    var step = -100;
    if (e.wheelDelta < 0) {
      step *= -1;
    }
    var newPos = window.pageXOffset + step;
    $('body').scrollLeft(newPos);
    console.log("Hola");
})

