import mysql.connector

class MetaSingleton(type):
    __instances = {}
    def __call__(cls,*args,**kwargs):
        if cls not in cls.__instances:
            cls.__instances[cls] = super(MetaSingleton, cls).__call__(*args,**kwargs)
        return cls.__instances[cls]

class DataBase(metaclass=MetaSingleton):
    connection = None

    def __init__(self,db):
        self.__db = db

    def connect(self):
        self.connection = mysql.connector.connect(
            host = 'localhost',
            user = 'root',
            passwd = '',
            database = self.__db
        )
        self.cursor = self.connection.cursor()
        qr = """CREATE TABLE if not exists users (
        id int(11) NOT NULL,
        username varchar(50) NOT NULL,
        password varchar(50) NOT NULL,
        email varchar(100) NOT NULL,
        primary key(id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"""
        self.cursor.execute(qr)
        qr = """create table if not exists permissions (
        id int(11) NOT NULL,
        user int(11) NOT NULL,
        insertPermit tinyint(1) NOT NULL DEFAULT '0',
        updatePermit tinyint(1) NOT NULL DEFAULT '0',
        deletePermit tinyint(1) NOT NULL DEFAULT '0',
        primary key(id),
        foreign key(id) references users(id) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"""
        self.cursor.execute(qr)
        self.cursor.execute("INSERT IGNORE INTO users values (1, 'admin', 'admin', 'alemoralesb11@gmail.com');")
        self.cursor.execute("INSERT IGNORE INTO permissions VALUES (1, 1, 1, 1, 1);")
        self.connection.commit()
        return self.connection

    def query(self,after_from,select='*'):
        self.cursor.execute("SELECT {} FROM {};".format(select,after_from))
        return self.cursor.fetchall()

    def delete(self,table,id):
        self.cursor.execute("DELETE FROM {} WHERE {}".format(table,id))
        self.connection.commit()

    def queri(self,table,new_value,id):
        self.cursor.execute("SELECT {} FROM {} WHERE {}".format(table,new_value,id))
        return self.cursor.fetchall()

    def update(self,table,new_value,id):
        self.cursor.execute("UPDATE {} SET {} WHERE {}".format(table,new_value,id))
        self.connection.commit()

    def get_columns(self,table):
        self.cursor.execute("SHOW COLUMNS FROM {}".format(table))
        columnas = self.cursor.fetchall()
        columns = []
        for i in columnas:
            columns.append(i[0])
        return columns
    
    def get_tables(self):
        self.cursor.execute("SHOW TABLES")
        tablas = self.cursor.fetchall()
        t = []
        for i in tablas:
            if i[0] != 'permissions' and i[0] != 'users':
                t.append(i[0])
        print(t)
        return t
    
class User_actions(metaclass=MetaSingleton):

    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def create(self,table,values):
        self.cursor.execute("INSERT INTO {} VALUES ({})".format(table,values))
        self.connection.commit()

    def delete(self,table,id):
        self.cursor.execute("DELETE FROM {} WHERE {}".format(table,id))
        self.connection.commit()
    
    def update(self,table,new_value,id):
        self.cursor.execute("UPDATE {} SET {} WHERE {}".format(table,new_value,id))
        self.connection.commit()
