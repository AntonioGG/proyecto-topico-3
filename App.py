from flask import Flask, render_template, request, redirect, url_for, flash
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from abc import abstractmethod, ABCMeta
import itertools
from random import randint
from statistics import mean
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas

import singleton
import pandas as pd
import os
import tablib

app = Flask(__name__)
dataset = tablib.Dataset()
# Mysql Connection
# Base de datos que se quiere usar
#db = singleton.DataBase("dw_proyecto")
db = singleton.DataBase("dren")
usr = db.connect()
usuario = singleton.User_actions(usr)

class user_log(metaclass=singleton.MetaSingleton):

    def __init__(self,user=None):
        self.__user = user

    def set_user(self,new_user):
        self.__user = new_user

    def get_user(self):
        return self.__user

    def close_session(self):
        self.__user = None

    def get_permissions(self):
        query = "permissions WHERE user = (SELECT id FROM users WHERE username = '{}')".format(self.__user)
        var = db.query(query, "insertPermit,updatePermit,deletePermit")
        print(var)
        return var

# settings
app.secret_key = "mysecretkey"

usr_log = user_log()

@app.route('/', methods=['POST','GET'])
def login():
    if request.method == 'POST':
        user = request.form['user']
        passw = request.form['pass']
        param = "{} where {} = '{}' and {} = '{}'".format('users', 'username', user, 'password', passw)
        ex=db.query(param)
        if (len(ex) is not 0):
            if ex[0][0] == 1:
                usr_log.set_user("Admin")  
                Index()
                return redirect(url_for('Index'))
            else:
                usr_log.set_user(str(user))
                return redirect(url_for('Index'))
        else:
            return render_template('login.html')
    else:
        return render_template('login.html')

@app.route('/cerrar', methods=['POST', 'GET'])
def cerrar():
    usr_log.close_session()
    return render_template('login.html')    

@app.route('/indexRegistro/', methods=['POST', 'GET'])
def registro():
    if(usr_log.get_user() == None):
        return render_template('login.html') 
    db.connect()
    task = db.query('users')
    ta=db.query('permissions')
    columns = db.get_columns('users')
    permission = []
    columns.append('crear')
    columns.append('editar')
    columns.append('eliminar')
    for t in ta:
        permission.append([t[2],t[3],t[4]])
    d = {
        'table':'users',
        'columns':columns,
        'task':task,
        'permission':permission,
        'user': usr_log.get_user(),
        'permissions': usr_log.get_permissions(),
        'tables': db.get_tables()
    }
    return render_template('indexRegistro.html',tasks=d)

@app.route('/index', methods=['POST', 'GET'])
def Index():
    if(usr_log.get_user() == None):
        return render_template('login.html') 
    d = {
        'tables':db.get_tables(),
        'user': usr_log.get_user()
        }
    return render_template('index.html',tasks=d)

@app.route('/indexCRUD/<string:table>', methods=['POST', 'GET'])
def CRUD(table):
    if(usr_log.get_user() == None):
        return render_template('login.html') 
    db.connect()
    if request.method == 'GET' and table != 'favicon.ico':
        t = {
            'task': db.query(table),
            'columns': db.get_columns(table),
            'table': table,
            'tables':db.get_tables(),
            'user': usr_log.get_user(),
            'permissions': usr_log.get_permissions()
        }
        return render_template('indexCRUD.html', tasks=t)

def verify(id,table,column):
    q = "{} where {} = {}".format(table,column,id)
    print(db.query(q,column))
    return False if db.query(q,column) == [] else True

@app.route('/create/<string:table>', methods=['POST', 'GET'])
def create(table):
    if(usr_log.get_user() == None):
        return render_template('login.html') 
    if table == 'users':
        columns = db.get_columns(table)
        col = db.get_columns('permissions')
        columns.append(col[2])
        columns.append(col[3])
        columns.append(col[4])
    else:
        columns = db.get_columns(table)   
    if request.method == 'POST':
        if (verify(request.form[columns[0]],table,columns[0])):
            flash("Id existente")
            return redirect(request.url)
        if table == 'users':
            value = ""
            for i in range((len(columns)-3)):
                r_form = request.form[columns[i]]
                if i == 0:
                    value += "{}".format(r_form) if r_form.isdigit() else "'{}'".format(r_form)
                else:
                    value += ", {}".format(r_form) if r_form.isdigit() else ", '{}'".format(r_form)
            usuario.create(table, value)
            flash('Created Successfully')
            vals = ""
            r_form =  request.form[columns[0]]
            vals += "{}".format(r_form) if r_form.isdigit() else "'{}'".format(r_form)
            r_form =  request.form[columns[0]]
            vals += ",{}".format(r_form) if r_form.isdigit() else ", '{}'".format(r_form)
            for j in range(4,(len(columns))):
                r_form =  request.form[columns[j]]
                if i == 0:
                    vals += "{}".format(r_form) if r_form.isdigit() else "'{}'".format(r_form)
                else:
                    vals += ", {}".format(r_form) if r_form.isdigit() else ", '{}'".format(r_form)
            usuario.create('permissions', vals)
            flash('Created Successfully')
            return redirect(url_for('registro', table=table))
        else:
            value = ""
            for i in range(len(columns)):
                r_form = request.form[columns[i]]
                if i == 0:
                    value += "{}".format(r_form) if r_form.isdigit() else "'{}'".format(r_form)
                else:
                    value += ", {}".format(r_form) if r_form.isdigit() else ", '{}'".format(r_form)
            usuario.create(table, value)
            flash('Created Successfully')
            return redirect(url_for('CRUD', table=table))
    elif request.method == 'GET' and table != 'favicon.ico':
        t = {
            'columns': columns,
            'table': table,
            'type': 'create',
            'tables':db.get_tables(),
            'user':usr_log.get_user()
        }
        return render_template('update.html', tasks=t)


@app.route('/delete/<string:table>/<id>', methods=['POST', 'GET'])
def delete(table, id):
    if(usr_log.get_user() == None):
        return render_template('login.html') 
    if table == 'users':
        c = db.get_columns(table)
        where = "{} = {}".format(c[0], id) if id.isdigit() else "{} = '{}'".format(c[0], id)
        usuario.delete(table, where)
        return redirect(url_for('registro', table=table))
    else:
        c = db.get_columns(table)
        where = "{} = {}".format(c[0], id) if id.isdigit() else "{} = '{}'".format(c[0], id)
        usuario.delete(table, where)
        flash('Contact Removed Successfully')
        return redirect(url_for('CRUD', table=table))

@app.route('/update/<string:table>/<id>', methods=['POST', 'GET'])
def update(table, id):
    if(usr_log.get_user() == None):
        return render_template('login.html') 
    if table == 'users':
        columns =[]
        column = db.get_columns(table)
        col = db.get_columns('permissions') 
        columns.append(column)
        columns.append([col[2],col[3],col[4]])
    else:
        columns = db.get_columns(table)   
    if request.method == 'POST':
        columns = db.get_columns(table)
        if (verify(request.form[columns[0]],table,columns[0])) and (id != request.form[columns[0]]):
            flash("Id existente")
            return redirect(request.url)
        else:
            if table == 'users':
                value = ""
                for i in range((len(columns))):
                    r_form = request.form[columns[i]]
                    if i == 0:
                        value += "{} = {}".format(str(columns[i]), r_form) if r_form.isdigit() else "{} = '{}'".format(
                            str(columns[i]), r_form)
                    else:
                        value += ", {} = {}".format(str(columns[i]), r_form) if r_form.isdigit() else ", {} = '{}'".format(
                            str(columns[i]), r_form)
                vals = ""
                for j in range(2,(len(col))):
                    r_form =  request.form[col[j]]
                    if j == 2:
                        vals += "{} = {}".format(str(col[j]), r_form) if r_form.isdigit() else "{} = '{}'".format(
                            str(col[i]), r_form)
                    else:
                        vals += ", {} = {}".format(str(col[j]), r_form) if r_form.isdigit() else ", {} = '{}'".format(
                            str(col[i]), r_form)

                id = "{} = {}".format(columns[0], id) if id.isdigit() else "{} = '{}'".format(columns[0], id)
                usuario.update(table, value, id)
                usuario.update('permissions', vals, id)
                flash('Updated Successfully')
                return redirect(url_for('registro', table=table))
            else:
                value = ""
                for i in range(len(columns)):
                    r_form = request.form[columns[i]]
                    if i == 0:
                        value += "{} = {}".format(str(columns[i]), r_form) if r_form.isdigit() else "{} = '{}'".format(
                            str(columns[i]), r_form)
                    else:
                        value += ", {} = {}".format(str(columns[i]), r_form) if r_form.isdigit() else ", {} = '{}'".format(
                            str(columns[i]), r_form)

                id = "{} = {}".format(columns[0], id) if id.isdigit() else "{} = '{}'".format(columns[0], id)

                usuario.update(table, value, id)
                flash('Updated Successfully')
                return redirect(url_for('CRUD', table=table))
    elif request.method == 'GET' and table != 'favicon.ico':
        if table == 'users':
            table2 = "{} where {} = {}".format(table, column[0], id) if id.isdigit() else "{} where {} = '{}'".format(
            table, column[0], id)
            task = db.query(table2)
            table3 = "{} where {} = {}".format('permissions', column[0], id) if id.isdigit() else "{} where {} = '{}'".format(
            table, column[0], id)
            task2 = db.query(table3)
            task.append([task2[0][2],task2[0][3],task2[0][4]])
            
        else:
            table2 = "{} where {} = {}".format(table, columns[0], id) if id.isdigit() else "{} where {} = '{}'".format(
            table, columns[0], id)
            task = db.query(table2)
            
        t = {
            'task': task,
            'columns': columns,
            'table': table,
            'type': 'update',
            'tables':db.get_tables(),
            'user':usr_log.get_user()
        }
        return render_template('update.html', tasks=t)


@app.route('/indexCSV/', methods=['POST', 'GET'])
def CSV():
    if(usr_log.get_user() == None):
        return render_template('login.html')

    if request.method == 'POST':
        url = request.form['url']
        print (url)
    url = url
    csv_print(url)
    data = dataset.html
    export_to_pdf(dataset)
    tables = db.get_tables()
    d = {
        'data':data,
        'tables':tables,
        'user':usr_log.get_user()
    }
    return render_template('indexCSV.html',tasks=d)

@app.route('/query/<string:table>', methods=['POST', 'GET'])
def query(table):
    if (usr_log.get_user() == None):
        return render_template('login.html')
    columns = db.get_columns(table)
    if request.method == 'POST':
        value = ""
        for i in range(len(columns)):
            r_form = request.form[columns[i]]
            if i == 0:
                value += "{}".format(r_form) if r_form.isdigit() else "'{}'".format(r_form)
            else:
                value += ", {}".format(r_form) if r_form.isdigit() else ", '{}'".format(r_form)
        db.query(table,"EXPEDIENTE")
        flash('Created Successfully')
        return redirect(url_for('CRUD', table=table ))
    elif request.method == 'GET' and table != 'favicon.ico':
        t = {
            'columns': columns,
            'table': table,
            'type': 'query',
            'user': usr_log.get_user()
        }
        return render_template('update.html', tasks=t)

def csv_print(url):
    mainpath = "C:/Users/garci/Documents/UAQ/7mo Semestre/Topico 3/ejemploCRUD/python-ml-course/notebooks/resources/datasets/"
    filename = "titanic/titanic3.csv"
    fullpath = os.path.join(mainpath, filename)
    print(fullpath)
    if url == "defecto":
        data3 = open(mainpath + "/" + "spanish-silver/spanish-silver.csv", 'r')
    else:
        data3 = open(url, 'r')
    dataset.csv = data3.read()
    cols = data3.readline().strip().split(",")
    n_cols = len(cols)
    counter = 0
    main_dict = {}
    for col in cols:
        main_dict[col] = []
    for line in data3:
        values = line.strip().split(",")
        for i in range(len(cols)):
            (main_dict[cols[i]].append(values[i]))
        counter += 1

    print("El data set tiene %d filas y %d columnas" % (counter, n_cols))
    df3 = pd.DataFrame(main_dict)
    print(df3.head(n_cols))


def grouper(iterable, n):
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args)

def export_to_pdf(data):
    c = canvas.Canvas("reporte.pdf", pagesize=A4)
    w, h = A4
    max_rows_per_page = 45
    # Margin.
    x_offset = 50
    y_offset = 50
    # Space between rows.
    padding = 15

    xlist = [x + x_offset for x in [0, 200, 250, 300, 350, 400, 480]]
    ylist = [h - y_offset - i * padding for i in range(max_rows_per_page + 1)]

    for rows in grouper(data, max_rows_per_page):
        rows = tuple(filter(bool, rows))
        c.grid(xlist, ylist[:len(rows) + 1])
        for y, row in zip(ylist[:-1], rows):
            for x, cell in zip(xlist, row):
                c.drawString(x + 2, y - padding + 3, str(cell))
        c.showPage()

    c.save()


if __name__ == "__main__":
    app.run(debug=True)

"""
    cols = data3.readline().strip().split(",")
    n_cols = len(cols)
    counter = 0
    main_dict = {}
    for col in cols:
        main_dict[col] = []
    for line in data3:
        values = line.strip().split(",")
        for i in range(len(cols)):
            (main_dict[cols[i]].append(values[i]))
        counter += 1
        
            print("El data set tiene %d filas y %d columnas" % (counter, n_cols))
    df3 = pd.DataFrame(main_dict)
    print(df3.head(n_cols))
    
"""